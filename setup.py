#!/usr/bin/env python

import os.path as op

from setuptools import setup, find_packages


basedir = op.dirname(__file__)


def read_version():
    version = {}
    with open(op.join(basedir, 'fsl', 'gui', 'bet', '__init__.py')) as f:
        for line in f:
            if line.startswith('__version__'):
                exec(line, version)
                break
    return version['__version__']


def read_requirements():
    return ['fsl-gui-core']


setup(name='fsl-gui-bet',
      description='',
      author='',
      version=read_version(),
      packages=find_packages(),
      install_requires=read_requirements(),
      entry_points={'console_scripts' : ['bet_server = fsl.gui.bet:main']})
