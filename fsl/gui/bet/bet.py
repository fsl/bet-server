#!/usr/bin/env python
#
# bet.py - The bet_server entry point.
#

import sys

import fsl.data.image as fslimage
import fsl.wrappers   as wrappers

import fsl.gui.server as fslserver


async def set_input(sio, sid, msg):
    """This handler is called when a message of type ``'input'`` is received.
    When the user selects an input file, a default output file name is
    generated and emitted via a message of type ``'output'``.
    """
    infile  = msg['filename']
    outfile = fslimage.removeExt(infile) + '_brain'

    await sio.emit('output', {'filename' : outfile}, room=sid)


async def run_bet(sio, sid, msg):
    """This handler is called when a message of type ``'run'`` is received.
    It expects to be passed all of the parameters required to run BET via the
    :func:`fsl.wrappers.bet` function.
    """
    # TODO make this non blocking
    cmd = wrappers.bet(**msg, v=True, cmdonly=True)
    await fslserver.run_command(sid, sio, cmd)


def main(argv=None):
    """Entry point. Starts the bet_server web server. Expects to be
    given a HTTP port number to listen on as the sole command-line argument.
    """
    if argv is None:
        argv = sys.argv[1:]

    if len(argv) == 0:
        port = None
    else:
        port = int(argv[0])

    handlers = {
        'input' : set_input,
        'run'   : run_bet,
    }

    fslserver.start_server(port=port, handlers=handlers)


if __name__ == '__main__':
    sys.exit(main())
