#!/usr/bin/env python

import sys
import functools as ft

import socketio


def main():

    server_url = sys.argv[1]
    infile     = sys.argv[2]

    sio = socketio.Client()

    def on_connect():
        print('Connected - sending input file')
        sio.emit('input', {'filename' : infile})

    def on_output_image(data):
        outfile = data['filename']
        print(f'Received output event {data}')
        sio.emit('run', {'input' : infile, 'output' : outfile})

    def on_output(data, msgtype):
        print(f'Received {msgtype} event:')
        print('  ', data['data'])

    def on_command(data):
        print('Received command event:')
        print('  ', data['command'])

    def on_finished(data):
        print('Received finished event')
        print('  exit code: ', data['exitcode'])
        print('  total time:', data['totaltime'])

        # don't disconnect until we're sure that the
        # server has received the shutdown event
        sio.emit('shutdown', {}, callback=on_shutdown)

    def on_shutdown():
        sio.disconnect()

    on_stdout = ft.partial(on_output, msgtype='stdout')
    on_stderr = ft.partial(on_output, msgtype='stderr')

    sio.on('connect', on_connect)
    sio.on('output',  on_output_image)
    sio.on('stdout',  on_stdout)
    sio.on('stderr',  on_stderr)
    sio.on('command', on_command)
    sio.on('success', on_finished)
    sio.on('error',   on_finished)

    sio.connect(server_url)
    sio.wait()

if __name__ == '__main__':
    sys.exit(main())
