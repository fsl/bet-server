# FSL BET GUI server

This directory contains the `fsl-gui-bet` Python library, which provides the
`bet_server` command that runs the back-end for the BET GUI.
